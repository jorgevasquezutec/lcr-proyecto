import RedisClientSingleton from './redisClient';

async function main() {
  const redisClient = RedisClientSingleton.getInstance();
  await redisClient.connect();

  const data = await redisClient.getListData('mi_lista');
  console.log(data);

  await redisClient.disconnect();
}

main().catch(err => console.error(err));